#!/usr/bin/python3

from nmodl import dsl

examples = dsl.list_examples()
nmodl_string = dsl.load_example(examples[-1])
driver = dsl.NmodlDriver()
modast = driver.parse_string(nmodl_string)

print('AST as JSON:')
print(dsl.to_json(modast))
print()
print('AST as NMODL:')
print(dsl.to_nmodl(modast))
